package ru.t1.ytarasov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    protected String token;

    public AbstractUserRequest(@Nullable String token) {
        this.token = token;
    }

}
