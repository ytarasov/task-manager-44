package ru.t1.ytarasov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
public class SessionDTO extends AbstractUserOwnedModelDTO {

    private static final long serialVersionUID = 1;

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role;

}
