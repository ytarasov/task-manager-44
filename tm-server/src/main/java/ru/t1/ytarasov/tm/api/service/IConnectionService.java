package ru.t1.ytarasov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    Connection getConnection();

    @NotNull
    SqlSession getSqlSession();

    @NotNull
    SqlSessionFactory getSqlSessionFactory();

    @NotNull
    EntityManagerFactory factory();

    @NotNull
    EntityManager getEntityManager();

}
