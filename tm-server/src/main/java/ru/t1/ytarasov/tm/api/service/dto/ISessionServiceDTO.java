package ru.t1.ytarasov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;

public interface ISessionServiceDTO extends IUserOwnedServiceDTO<SessionDTO> {
}
