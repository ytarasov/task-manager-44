package ru.t1.ytarasov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;

import java.util.Comparator;
import java.util.List;

public interface IProjectServiceDTO extends IUserOwnedServiceDTO<ProjectDTO> {

    @Nullable
    List<ProjectDTO> findAll(@Nullable Comparator comparator) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(@Nullable Sort sort) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception;

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    ProjectDTO changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception;

}