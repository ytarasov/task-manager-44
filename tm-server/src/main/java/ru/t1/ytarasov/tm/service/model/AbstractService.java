package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.repository.model.IRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.model.IService;
import ru.t1.ytarasov.tm.model.AbstractModel;

import javax.persistence.EntityManager;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final EntityManager entityManager;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
        this.entityManager = connectionService.getEntityManager();
    }
}
